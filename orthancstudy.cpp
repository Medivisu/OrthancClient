#include "orthancstudy.h"

#include "orthancclientexception.h"

#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>


#include "orthancclientexception.h"
#include "toolbox.h"

namespace orthanc_client
{

const char OrthancStudy::ACCESSION_NUMBER[]   = "AccessionNumber";
const char OrthancStudy::STUDY_DATE[]         = "StudyDate";
const char OrthancStudy::STUDY_DESCRIPTION[]  = "StudyDescription";
const char OrthancStudy::STUDY_ID[]           = "StudyID";
const char OrthancStudy::STUDY_INSTANCE_UID[] = "StudyInstanceUID";
const char OrthancStudy::STUDY_TIME[]         = "StudyTime";

/*!
 * \brief Creates a new study provided by Orthanc.
 * \param connection the Orthanc connection.
 * \param id the Orthanc id.
 */
OrthancStudy::OrthancStudy(orthanc_client::OrthancConnection & connection,
             const QString & id) :
    IOrthancResource(connection, id),
    m_series(*this)
{
    m_series.setThreadCount(m_connection.threadCount());
    readStudy();
}

/*!
 * \brief Get the number of series.
 * \return the number of series.
 */
int OrthancStudy::getSerieCount()
{
    return m_series.getSize();
}

/*!
 * \brief Get the serie at the given index.
 * \param index the index.
 * \return the serie at the given index.
 */
OrthancSerie & OrthancStudy::getSerie(int index)
{
    return dynamic_cast<OrthancSerie&> (m_series.getItem(index));
}

/*!
 * \brief Returns the serie by ID.
 *
 * This method is longer than getStudy(int index). It searches all the series
 * until it finds the right ID. If the ID has not been found, an exception is
 * thrown.
 * \param id the id.
 * \return the serie.
 * \throw OrthancClientException if no serie was found.
 */
OrthancSerie & OrthancStudy::getSerieByID(const QString& id)
{
    int i = 0;
    while (i < getSerieCount() && getSerie(i).getOrthancID() != id)
        ++i;

    if (i >= getSerieCount())
        throw OrthancClientException("No serie found : no serie with the id : " + id);

    return getSerie(i);
}

/*!
 * \brief Download an archive of the study with all the series / instances.
 * \param path the directory path where the archive will be saved.
 * \param zipName the archive name.
 * \throw OrthancClientException if the download fails, or if you cannot write
 * to the specified directory / path.
 */
void OrthancStudy::downloadAsZip(const QString & path, QString zipName)
{
    IOrthancResource::downloadAsZip(path, zipName, "/studies/");
}

/*!
 * \brief Download an archive of the study with all the series / instances.
 * This method will give the Orthanc ID as the name of the archive.
 * If you want to specify your own name, use the overloaded function :
 * downloadAsZip(const QString& path, const QString& zipName)
 * \param path the directory path where the archive will be saved.
 * \throw OrthancClientException if the download fails, or if you cannot write
 * to the specified directory / path.
 */
void OrthancStudy::downloadAsZip(const QString & path)
{
    downloadAsZip(path, m_id + ".zip");
}

void OrthancStudy::deleteResource()
{
    IOrthancResource::deleteResource("/studies/");
}

/*!
 * \brief Copy the study and anonymize it, returns the new study orthanc ID.
 *
 * The studies will not be updated until you reload the connection yourself.
 *
 * \return the new study Orthanc ID.
 */
QString OrthancStudy::anonymize(bool reload)
{
    return IOrthancResource::anonymize("/studies/", reload);
}

QString OrthancStudy::anonymize(const QVariantMap & replace,
                                  const QStringList & keep,
                                  const QStringList & remove,
                                  bool keepPrivateTags)
{
    return IOrthancResource::anonymize(replace, keep,
                                       remove, keepPrivateTags,
                                       "/studies/");
}

// ========== PRIVATE METHODS ========== //

// Read the study
void OrthancStudy::readStudy()
{
    http::HttpClient client = m_connection.httpClient();
    client.setMethod(http::HttpMethod::GET);
    client.setUrl(m_connection.url().toString() + "/studies/" + m_id);

    if (!client.apply(m_content))
        throw orthanc_client::OrthancClientException(client.lastError());
}

// Get the number of series
int OrthancStudy::getFillerSize()
{
    return m_content.object()["Series"].toArray().size();
}

// Get the serie with the given index.
core::IDynamicObject * OrthancStudy::getFillerItem(int index)
{
    QString id = m_content.object()["Series"].toArray()[index].toString();
    return new OrthancSerie(m_connection, id);
}

}


