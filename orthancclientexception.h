#ifndef ORTHANCCLIENTEXCEPTION
#define ORTHANCCLIENTEXCEPTION

#include <QString>

namespace orthanc_client
{

class OrthancClientException
{

        QString m_what;

    public:
        OrthancClientException(const QString& what = "OrthancClientException")
            : m_what(what) {}

        const QString& what()
        {
            return m_what;
        }

};

}

#endif // ORTHANCCLIENTEXCEPTION

