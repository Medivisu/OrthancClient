#ifndef ORTHANCCONFIGURATION
#define ORTHANCCONFIGURATION

#include <boost/noncopyable.hpp>

#include <QUrl>
#include <QJsonDocument>

#include "multithreading/arrayfilledbythreads.h"

#include "httpclient.h"



namespace orthanc_client
{

class OrthancPatient;

/*!
 * \brief The OrthancConnection is a connection to an Orthanc Server.
 * It allows you to retrive informations from the server.
 */
class OrthancConnection :
        public boost::noncopyable,
        private core::ArrayFilledByThreads::IFiller
{

    public:
        OrthancConnection(const QString& orthancUrl = "http://localhost:8042");

        OrthancConnection(const QString& orthancUrl,
                          const QString& username,
                          const QString& password);

        virtual ~OrthancConnection() = default;

        inline const QUrl& url() const;
        inline void setUrl(const QUrl & url);
        inline void setUrl(const QString & url);
        inline void setHost(const QString & host);
        inline void setPort(int port);

        inline const http::HttpClient& httpClient() const;

        inline int threadCount() const;
        inline void setThreadCount(int threadCount);

        int getPatientCount();
        OrthancPatient& getPatient(int index);
        OrthancPatient& getPatientByOrthancID(const QString& id);

        void deletePatient(int index);
        void store(const QByteArray& orthanc_client);
        void storeFile(const QString& dicomPath);

        void reload();



    private:
        http::HttpClient m_client;
        QJsonDocument m_content;
        core::ArrayFilledByThreads m_patients;
        QUrl m_url;
        QString m_username;
        QString m_password;

        // Private methods
        void readPatients();
        virtual int getFillerSize();
        virtual core::IDynamicObject * getFillerItem(int index);
};

// Inline methods

/*!
 * \brief Returns the url of the Orthanc server.
 * \return the url of the Orthanc server.
 */
const QUrl &OrthancConnection::url() const
{
    return m_url;
}

/*!
 * \brief Set the url of the Orthanc server.
 *
 * The url is only set if the url is valid.
 *
 * \param url the url of the Orthanc server.
 */
void OrthancConnection::setUrl(const QUrl & url)
{
    if (url.isValid())
        m_url = url;
}

/*!
 * \brief Set the url of the Orthanc server.
 *
 * The url is only set if the url is valid.
 *
 * \param url the url of the Orthanc server.
 */
void OrthancConnection::setUrl(const QString & url)
{
    setUrl(QUrl(url));
}

/*!
 * \brief Set the host of the Orthanc server URL.
 * \param host the host of the Orthanc server URL.
 */
void OrthancConnection::setHost(const QString & host)
{
    m_url.setHost(host);
}

/*!
 * \brief Set the port of the Orthanc server URL.
 * \param port the port of the Orthanc server URL.
 */
void OrthancConnection::setPort(int port)
{
    m_url.setPort(port);
}

/*!
 * \brief Returns the Http Client of the connection.
 * \return the Http Client of the connection.
 */
const http::HttpClient & OrthancConnection::httpClient() const
{
    return m_client;
}

/*!
 * \brief Returns the number of threads used to download informations
 * from the instance of Orthanc.
 * \return the number of threads used to download informations
 * from the instance of Orthanc.
 */
int OrthancConnection::threadCount() const
{
    return m_patients.threadCount();
}

/*!
 * \brief Sets the number of threads used to download informations
 * from the instance of Orthanc.
 * \param threadCount the number of threads used to download informations
 * from the instance of Orthanc.
 */
void OrthancConnection::setThreadCount(int threadCount)
{
    m_patients.setThreadCount(threadCount);
}

} // namespace OrthancClient



#endif // ORTHANCCONFIGURATION

