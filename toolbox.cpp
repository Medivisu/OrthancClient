#include "toolbox.h"

#include <QDir>
#include <QFile>
#include <QProcess>
#include <QCryptographicHash>
#include <QDebug>
#include <QDataStream>


/*!
 * \brief Returns true if the file with the given path exists, false otherwise.
 * \param path the path of the file to check.
 * \return true if the file with the given path exists, false otherwise.
 */
bool toolbox::isExistingFile(const QString & path)
{
    return QFile::exists(path);
}

/*!
 * \brief Returns true if the directory (path) exists, false otherwise.
 * \param path the path to check.
 * \return true if the directory (path) exists, false otherwise.
 */
bool toolbox::isExistingPath(const QString & path)
{
    QDir dir(path);
    return dir.exists();
}


/*!
 * \brief Read a text file with a given path.
 *
 * This method is made for reading text files. Use the other method
 * to read binary data with a QByteArray.
 * The content is cleared before reading.
 * \param content the content of the file.
 * \param path the file path.
 * \return true if the file has been correctly read, false otherwise.
 */
bool toolbox::readFile(QString & content, const QString & path)
{
    content.clear(); // Clear the previous content
    QFile file(path);

    bool openable = file.open(QFile::ReadOnly | QFile::Text);
    if (openable)
    {
        content = QString(file.readAll());
        file.close();
    }

    return openable;
}

/*!
 * \brief Read a file with a given path.
 *
 * \param content the content of the file.
 * \param path the file path.
 * \return true if the file has been correctly read, false otherwise.
 */
bool toolbox::readFile(QByteArray & content, const QString & path)
{
    content.clear();
    QFile file(path);

    bool openable = file.open(QFile::ReadOnly);
    if (openable)
    {
        content = file.readAll();
        file.close();
    }

    return openable;
}

/*!
 * \brief Write some content to a file with a given path.
 *
 * The filename is included to the path in this method.
 *
 * If the path does not exists, the function returns false and does not write
 * the new file.
 *
 * If the length of the written bytes are equal to the content size, it means
 * the file has been successfully written.
 *
 * \param content the content to be written to the file.
 * \param path the path of the file to be written.
 * \return true if the file has been successfully written, false otherwise.
 */
bool toolbox::writeFile(const QByteArray & content, const QString & path)
{
    QFile file(path);

    int size;
    bool openable = file.open(QFile::WriteOnly);
    if (openable)
    {
        size = file.write(content);
        file.close();
    }

    return openable && (size == content.size());
}

/*!
 * \brief This is an overloaded function, the filename is not included to
 * the path parameter.
 * \param content the content to be written to the file.
 * \param path the path of the directory.
 * \param filename the name of the file to be written.
 * \return true if the file has been successfully written, false otherwise.
 */
bool toolbox::writeFile(const QByteArray & content, const QString & path, const QString & filename)
{
    QDir dir(path);
    if (dir.exists())
        return writeFile(content, dir.absolutePath() + "/" + filename);
    else
        return false;
}

/*!
 * \brief Writes a list of binary data into a directory, with given filenames.
 *
 * The size of the filename list must be equal to the size of the binary
 * data list. If the method fails, some files may have been written with an invalid
 * state.
 *
 * \param path the path of the directory.
 * \param binaryData the data list.
 * \param filenames the filenames.
 * \return true if success, false otherwise.
 */
bool toolbox::writeFilesToDirectory(const QString & path,
                                    QVector<QByteArray> binaryData,
                                    QStringList filenames)
{
    bool success = true;
    int i = -1;

    QDir dir(path);
    if (!dir.exists() || filenames.size() != binaryData.size())
        success = false;

    while (success && ++i < binaryData.size())
        success = writeFile(binaryData.at(i), dir.absolutePath() + "/" + filenames.at(i));

    return success;
}

/*!
 * \brief Delete a file from the file system with a given path.
 *
 * If the path does not exsists, the function returns false.
 *
 * \param path the path of the file to be deleted.
 * \return true if the file has been successfully deleted, false otherwise.
 */
bool toolbox::removeFile(const QString & path)
{
    QFile file(path);
    return file.remove();
}

/*!
 * \brief Returns the size of a file.
 *
 * If the path is incorrect, the size returned will be incorrect.
 *
 * \param path the path of the file.
 * \return the size of the file.
 */
qint64 toolbox::getFileSize(const QString & path)
{
    QFile file(path);
    return file.size();
}


/*!
 * \brief Creates a new directory with a given path.
 *
 * If the directory already exists, returns true.
 * \param path the path of the new directory.
 * \return true if the directory has been successfully created, false otherwise.
 */
bool toolbox::createDirectory(const QString & path)
{
    QDir dir;
    return dir.mkdir(path) || dir.exists(path);
}

/*!
 * \brief Creates a new path (sequence of directory).
 * \param path the path to be created.
 * \return true if the new path has been successfully created, false otherwise.
 */
bool toolbox::createPath(const QString & path)
{
    QDir dir;
    return dir.mkpath(path);
}


/*!
 * \brief Returns true if the dicom is well formed.
 * Warning : A dicom file can be readable but not well formed !
 *
 * This function just verify if the picom prefix is in the file. Even a text file
 * could have this prefix. Use with caution.
 *
 * \param path the path of the dicom file.
 * \return true if the dicom file is well formed.
 */
bool toolbox::isWellFormedDicom(const QString & path)
{
    QFile file(path);
    if (!file.open(QFile::ReadOnly))
        return false;

    file.seek(128);
    QByteArray prefix = file.read(4);
    file.close();
    return prefix == "DICM";
}

/*!
 * \brief Returns true if the binary data of the dicom is well formed.
 * If this file is not a dicom file, it will return false.
 *
 * This function just verify if the picom prefix is in the file. Even a text file
 * could have this prefix. Use with caution.
 *
 * \param data the binary data of the dicom file.
 * \return true if the dicom is well formed.
 */
bool toolbox::isWellFormedDicom(const QByteArray & data)
{
    if (data.size() < 132)
        return false; // if the file is too small.

    QByteArray prefix;
    for (int i = 128; i < 132; ++i)
        prefix.append(data.at(i));

    return prefix == "DICM";
}

