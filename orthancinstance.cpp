#include "orthancinstance.h"

#include <QJsonObject>
#include <QJsonValue>
#include "orthancclientexception.h"
#include "assert.h"

#include <QDebug>
#include <QImage>
#include <QRgb>

namespace orthanc_client
{

const char OrthancInstance::ACQUISITION_NUMBER[]     = "AcquisitionNumber";
const char OrthancInstance::INSTANCE_CREATION_DATE[] = "InstanceCreationDate";
const char OrthancInstance::INSTANCE_CREATION_TIME[] = "InstanceCreationTime";
const char OrthancInstance::INSTANCE_NUMBER[]        = "InstanceNumber";
const char OrthancInstance::SOP_INSTANCE_UID[]       = "SOPInstanceUID";

/*!
 * \brief Creates a new Instance provided by Orthanc.
 * \param connection the Orthanc connection.
 * \param id the orthanc id.
 */
OrthancInstance::OrthancInstance(orthanc_client::OrthancConnection & connection,
                                 const QString & id) :
    IOrthancResource(connection, id),
    m_mode(ImageExtractionMode::Preview)
{
    readInstance();
    readSimplifiedTags();
}

/*!
 * \brief Returns a value with the given tag name as an integer.
 * \param tag the tag.
 * \return a value with the given tag name as an integer.
 */
int OrthancInstance::getTagAsInt(const QString & tag) const
{
    bool ok;
    float v = getTagAsVariant(tag).toInt(&ok);

    if (!ok)
        throw orthanc_client::OrthancClientException("The value cannot be converted to int.");

    return v;
}

/*!
 * \brief Returns a value with the given tag name as a real number.
 * \param tag the tag.
 * \return a value with the given tag name as a real number.
 */
float OrthancInstance::getTagAsFloat(const QString & tag) const
{
    bool ok;
    float v = getTagAsVariant(tag).toFloat(&ok);

    if (!ok)
        throw orthanc_client::OrthancClientException("The value cannot be converted to float.");

    return v;
}

/*!
 * \brief Returns a value with the given tag name as a string.
 * \param tag the tag.
 * \return a value with the given tag name as a string.
 */
QString OrthancInstance::getTagAsString(const QString & tag) const
{
    return getTagAsVariant(tag).toString();
}

/*!
 * \brief Returns a value with the given tag name as a variant.
 * \param tag the tag.
 * \return a value with the given tag name as a variant.
 */
QVariant OrthancInstance::getTagAsVariant(const QString & tag) const
{
    QJsonObject obj = m_simplifiedTags.object();
    if (!obj.contains(tag))
        throw orthanc_client::OrthancClientException("Tag: " + tag + " does not exists");

    return obj.value(tag).toVariant();
}

/*!
 * \brief Load a value from the tag name.
 * \param path the tag name.
 */
void OrthancInstance::loadTagContent(const QString & path)
{
    http::HttpClient client = m_connection.httpClient();
    client.setMethod(http::HttpMethod::GET);
    client.setUrl(m_connection.url().toString() + "/instances/" + m_id + "/content/" + path);

    if (!client.apply(m_tagContent))
        throw orthanc_client::OrthancClientException(client.lastError());
}

/*!
 * \brief Returns the loaded value of the previously loaded tag.
 * \return the loaded value of the previously loaded tag.
 */
QString OrthancInstance::getLoadedTagContent() const
{
    return QString(m_tagContent);
}

/*!
 * \brief Get all the tags-values as a variant map.
 * \return all the tags-values as a variant map.
 */
QVariantMap OrthancInstance::getAllTags()
{
    return m_simplifiedTags.object().toVariantMap();
}


/*!
 * \brief Returns the size of the dicom file.
 * \return the size of the dicom file.
 */
qint64 OrthancInstance::getDicomSize()
{
    downloadDicom();
    if (m_dicom.isNull())
        return -1;

    return m_dicom->size();
}

/*!
 * \brief Returns the binary data of the dicom file.
 * \return the binary data of the dicom file.
 */
const QByteArray& OrthancInstance::getDicom()
{
    downloadDicom();
    return *(m_dicom.data());
}

/*!
 * \brief Get the binary data of the downloaded image.
 * \return the binary date of the downloaded image.
 */
const QByteArray& OrthancInstance::getImageData()
{
    downloadImage();
    return *(m_imageData.data());
}

/*!
 * \brief Get the binary data of the line (at given index) of the downloaded
 * image.
 * \param line the index of the line.
 * \return the binary data of the line (at given index) of the downloaded
 * image.
 */
QByteArray OrthancInstance::getImageDataLine(int line)
{
    downloadImage();
    int bytesPerLines = m_image.bytesPerLine();
    char * data = (char *) malloc(bytesPerLines);
    QDataStream stream(*m_imageData.data());

    stream.skipRawData(line * bytesPerLines);
    stream.readRawData(data, bytesPerLines);

    return QByteArray(data, bytesPerLines);
}

/*!
 * \brief Discard the dicom data.
 */
void OrthancInstance::discardDicom()
{
    m_dicom.reset();
}

/*!
 * \brief Discard the image data.
 */
void OrthancInstance::discardImage()
{
    m_imageData.reset();
}

/*!
 * \brief Sets the image extraction mode.
 * \param mode the image extraction mode.
 */
void OrthancInstance::setImageExtractionMode(ImageExtractionMode mode)
{
    if (m_mode == mode)
        return;

    discardImage();
    m_mode = mode;
}

/*!
 * \brief Returns the pixel format.
 * \return the pixel format.
 */
QPixelFormat OrthancInstance::pixelFormat()
{
    downloadImage();
    return m_image.pixelFormat();
}

/*!
 * \brief Returns the image height.
 * \return the image height.
 */
int OrthancInstance::getHeight()
{
    downloadImage();
    return m_image.height();
}

/*!
 * \brief Returns the image width.
 * \return the image width.
 */
int OrthancInstance::getWidth()
{
    downloadImage();
    return m_image.width();
}

/*!
 * \brief Splits a value into a vector of float.
 * \param vector the vector of float.
 * \param tag the tag.
 */
void OrthancInstance::splitIntoVectorOfFloats(QVector<float> & vector, const QString & tag)
{
    QString value = getTagAsString(tag);

    QStringList values = value.split("\\");


    vector.clear();

    foreach (const auto& value, values)
    {
        bool ok;
        vector.append(value.toFloat(&ok));
        if (!ok)
            throw orthanc_client::OrthancClientException("Unable to parse the value");
    }
}

/*!
 * \brief Copy the instance and anonymize it, returns the new dicom file.
 * If you want to copy the instance of a serie in the orthanc server, anonymize
 * the all serie.
 *
 * \return the content of the new dicom file.
 */
QString OrthancInstance::anonymize(bool reload)
{
    return IOrthancResource::anonymize("/instances/", reload);
}

QString OrthancInstance::anonymize(const QVariantMap & replace,
                                      const QStringList & keep,
                                      const QStringList & remove,
                                      bool keepPrivateTags)
{
    return IOrthancResource::anonymize(replace, keep, remove, keepPrivateTags, "/instances/");
}

/*!
 * \brief Deletes the instance from the Orthanc Server.
 * The content you already get will be invalidated.
 *
 * \throw OrthancClientException if the Http Client fails the delete request.
 */
void OrthancInstance::deleteResource()
{
    IOrthancResource::deleteResource("/instances/");
}

// PRIVATE METHODS

void OrthancInstance::downloadImage()
{
    if (!m_imageData.isNull())
        return;

    QString suffix;

    switch (m_mode)
    {
        case ImageExtractionMode::Preview:
            suffix = "/preview";
            break;

        case ImageExtractionMode::UInt8:
            suffix = "/image-uint8";
            break;

        case ImageExtractionMode::UInt16:
            //suffix = "/image-uint16"; Don't work
            suffix = "/image-uint8";
            break;

        case ImageExtractionMode::Int16:
            //suffix = "/image-int16"; // Don't work.
            suffix = "/image-uint8";
            break;

        default:
            throw "Not implemented";
    }

    m_imageData.reset(new QByteArray());

    http::HttpClient client (m_connection.httpClient());
    client.setMethod(http::HttpMethod::GET);
    client.setUrl(m_connection.url().toString() + "/instances/" + m_id + suffix);

    if (!client.apply(*m_imageData.data()))
        throw orthanc_client::OrthancClientException(client.lastError());

    m_image = QImage::fromData(*m_imageData.data(), "PNG");
}

void OrthancInstance::downloadDicom()
{
    qDebug() << m_dicom.isNull();
    if (m_dicom.isNull())
    {
        http::HttpClient client = m_connection.httpClient();
        client.setUrl(m_connection.url().toString() + "/instances/" + m_id + "/file");
        client.setMethod(http::HttpMethod::GET);

        m_dicom.reset(new QByteArray());

        if (!client.apply(*m_dicom.data()))
            throw orthanc_client::OrthancClientException(client.lastError());
    }
}

void OrthancInstance::readInstance()
{
    http::HttpClient client = m_connection.httpClient();
    client.setMethod(http::HttpMethod::GET);
    client.setUrl(m_connection.url().toString() + "/instances/" + m_id);

    if (!client.apply(m_content))
        throw orthanc_client::OrthancClientException(client.lastError());
}

void OrthancInstance::readSimplifiedTags()
{
    http::HttpClient client = m_connection.httpClient();
    client.setMethod(http::HttpMethod::GET);
    client.setUrl(m_connection.url().toString() + "/instances/" + m_id + "/simplified-tags");

    if (!client.apply(m_simplifiedTags))
        throw orthanc_client::OrthancClientException(client.lastError());
}



}
