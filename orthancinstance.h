#ifndef INSTANCE_H
#define INSTANCE_H

#include "orthancconnection.h"
#include "enumerations.h"
#include "iorthancresource.h"

#include <QVariant>
#include <QScopedPointer>
#include <QPixelFormat>
#include <QImage>

namespace orthanc_client
{

/*!
 * \brief The instance class in the OrthancClient namespace corresponds to a
 * instance stored in an Orthanc Server.
 */
class OrthancInstance : public IOrthancResource
{

    public:

        // The main tags
        static const char ACQUISITION_NUMBER[];
        static const char INSTANCE_CREATION_DATE[];
        static const char INSTANCE_CREATION_TIME[];
        static const char INSTANCE_NUMBER[];
        static const char SOP_INSTANCE_UID[];

        OrthancInstance(orthanc_client::OrthancConnection& connection,
                        const QString& id);

        int getTagAsInt(const QString& tag) const;
        float getTagAsFloat(const QString& tag) const;
        QString getTagAsString(const QString& tag) const;
        QVariant getTagAsVariant(const QString& tag) const;

        void loadTagContent(const QString& path);
        QString getLoadedTagContent()  const;

        QVariantMap getAllTags();

        QString anonymize(bool reload);
        QString anonymize(const QVariantMap & replace,
                             const QStringList & keep,
                             const QStringList & remove,
                             bool keepPrivateTags);


        qint64 getDicomSize();
        const QByteArray& getDicom();
        const QByteArray& getImageData();

        void discardDicom();
        void discardImage();

        void setImageExtractionMode(ImageExtractionMode mode);
        QPixelFormat pixelFormat();

        int getHeight();
        int getWidth();

        void splitIntoVectorOfFloats(QVector<float>&vector, const QString& tag);

        QByteArray getImageDataLine(int line);
        void deleteResource() override;

    private:
        QJsonDocument m_simplifiedTags;
        QByteArray m_tagContent;
        QScopedPointer<QByteArray> m_dicom;
        QScopedPointer<QByteArray> m_imageData;
        QImage m_image;
        ImageExtractionMode m_mode;



        void downloadImage();
        void downloadDicom();
        void readInstance();
        void readSimplifiedTags();

};

}

#endif // INSTANCE_H
