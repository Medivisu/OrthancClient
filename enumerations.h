#ifndef ENUMERATIONS_H
#define ENUMERATIONS_H

enum class ImageExtractionMode
{
    /*!
     * \brief Rescaled to 8bpp.
     * The minimum value of the image is set to 0, and its maximum value is set to 255.
     **/
    Preview,

    /*!
     * \brief Truncation to the [0, 255] range.
     **/
    UInt8,

    /*!
     * \brief Truncation to the [0, 65535] range.
     **/
    UInt16,

    /*!
     * \brief Truncation to the [-32768, 32767] range.
     **/
    Int16
};



#endif // ENUMERATIONS_H

