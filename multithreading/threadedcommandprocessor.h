#ifndef THREADEDCOMMANDPROCESSOR_H
#define THREADEDCOMMANDPROCESSOR_H


#include <QMutex>
#include <QWaitCondition>

#include <QVector>
#include <QFuture>
#include "sharedmessagequeue.h"


namespace core
{

class ICommand;
class Processor;

class ThreadedCommandProcessor
{

    public:
        class IListener
        {
            public:
                virtual ~IListener() {}

                virtual void signalProgress(int current, int total) = 0;
                virtual void signalSuccess(int total) = 0;
                virtual void signalFailure() = 0;
                virtual void signalCancel() = 0;
        };

        ThreadedCommandProcessor(int numThreads);
        ~ThreadedCommandProcessor();

        void post(ICommand * command);
        bool join();
        void cancel();

        void setListener(IListener& listener);
        IListener& listener() const;


    private:
        friend class Processor;

        SharedMessageQueue m_queue;
        bool m_done;
        bool m_cancel;
        bool m_success;
        mutable QMutex m_mutex;
        IListener * m_listener;

        int m_remainingCommands;
        int m_totalCommands;
        QVector<Processor *> m_processors;
        QWaitCondition m_processedCommand;
};

}

#endif // THREADEDCOMMANDPROCESSOR_H
