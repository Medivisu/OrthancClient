#ifndef SHAREDMESSAGEQUEUE_H
#define SHAREDMESSAGEQUEUE_H

#include "boost/noncopyable.hpp"

#include <QLinkedList>
#include <QMutex>
#include <QWaitCondition>

#include "../idynamicobject.h"

namespace core
{

class SharedMessageQueue : public boost::noncopyable
{
    public:
        explicit SharedMessageQueue(int maxSize = 0);
        ~SharedMessageQueue();

        void enqueue(IDynamicObject * message);
        IDynamicObject * dequeue(qint32 millisecondsTimeout);
        bool waitEmpty(qint32 millisecondsTimeout);
        bool isFifoPolicy() const;
        bool isLifoPolicy() const;
        void setFifoPolicy();
        void setLifoPolicy();

    private:

        QLinkedList<IDynamicObject *> m_queue;
        mutable QMutex m_mutex;
        QWaitCondition m_elementAvailable;
        QWaitCondition m_emptied;
        bool m_isFifo;
        int m_maxSize;
};


}

#endif // SHAREDMESSAGEQUEUE_H
