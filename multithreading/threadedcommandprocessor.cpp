#include "threadedcommandprocessor.h"

#include <QMutexLocker>
#include <QScopedPointer>
#include <QThread>

#include "../commands/icommand.h"
#include "private/processor.h"


namespace core
{

/*!
 * \brief Creates a new threaded command processor.
 * \param numThreads the number of threads
 */
ThreadedCommandProcessor::ThreadedCommandProcessor(int numThreads)
{
    if (numThreads < 1)
        throw std::invalid_argument("You cannot set less than one thread");

    m_listener = nullptr;
    m_success = true;
    m_done = false;
    m_cancel = false;
    m_remainingCommands = 0;
    m_totalCommands = 0;

    // Reserve, not resize !
    m_processors.reserve(numThreads);

    for (int i = 0; i < numThreads; ++i)
    {
        m_processors.append(new Processor(this));
        m_processors.back()->start();
    }
}

ThreadedCommandProcessor::~ThreadedCommandProcessor()
{
    m_done = true;

    foreach (Processor * t, m_processors)
    {
        while (t->isRunning()); // Waits for the thread to quit.
        if (t)
            t->deleteLater();
    }
}

/*!
 * \brief Post a new command.
 * \param command the command.
 */
void ThreadedCommandProcessor::post(ICommand * command)
{
    if (!command)
        throw std::invalid_argument("You cannot post a null command");

    QMutexLocker locker(&m_mutex);
    m_queue.enqueue(command);
    ++m_remainingCommands;
    ++m_totalCommands;
}

/*!
 * \brief Waits for all the threads to process the remaining commands.
 * \return true if all the the commands have ended.
 */
bool ThreadedCommandProcessor::join()
{
    QMutexLocker locker(&m_mutex);

    // While all the commands are not finished
    while (m_remainingCommands != 0)
        m_processedCommand.wait(&m_mutex);

    if (m_cancel && m_listener)
        m_listener->signalCancel();

    // Reset the sequence counters for subsequent commands
    bool hasSucceeded = m_success;
    m_success = true;
    m_totalCommands = 0;
    m_cancel = false;

    return hasSucceeded;
}

/*!
 * \brief Cancels the processor.
 */
void ThreadedCommandProcessor::cancel()
{
    QMutexLocker locker(&m_mutex);
    m_cancel = true;
}

/*!
 * \brief Sets a listener.
 * \param listener the listener.
 */
void ThreadedCommandProcessor::setListener(ThreadedCommandProcessor::IListener & listener)
{
    QMutexLocker locker(&m_mutex);
    m_listener = &listener;
}

/*!
 * \brief Returns the listener.
 * \return the listener.
 */
ThreadedCommandProcessor::IListener & ThreadedCommandProcessor::listener() const
{
    return *m_listener;
}

} // namespace core
