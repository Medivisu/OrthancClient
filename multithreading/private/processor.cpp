#include "processor.h"

#include "../threadedcommandprocessor.h"
#include "../../commands/icommand.h"

#include <QThread>

namespace core
{

/*!
 * \brief The timeout in ms for the dequeue.
 */
const qint32 TIMEOUT = 10;

/*!
 * \brief Creates a new processor for the threaded command processor.
 * \param that The ThreadedCommandProcessor.
 */
Processor::Processor(ThreadedCommandProcessor * that) :
    m_that(that),
    m_thread(new QThread()),
    m_finished(false)
{
    this->moveToThread(m_thread);
    connect(m_thread, &QThread::started, this, &Processor::run);
    connect(this, &Processor::finished, this, [this] {
        m_thread->quit();
        m_finished = true;
    });
    connect(m_thread, &QThread::finished, m_thread, &QThread::deleteLater);
}

/*!
 * \brief Starts the processor.
 */
void Processor::start()
{
    m_thread->start();
}

/*!
 * \brief Returns true if the processor is running, false otherwise.
 *
 * This function will return true if the processor has not started yet.
 *
 * \return true if the processor is running, false otherwise.
 */
bool Processor::isRunning() const
{
    return !m_finished;
}

// The run method (main method for the processor).
void Processor::run()
{
    while (!m_that->m_done) // If the processor is not done running
    {
        // Take ownership of a command
        QScopedPointer<IDynamicObject> command(m_that->m_queue.dequeue(TIMEOUT));
        if (!command.isNull())
        {
            bool success = false;

            try
            {
                if (m_that->m_success)
                {
                    // No command has failed so far

                    // The  commands have been canceled. Skip execution
                    // of this command, yet mark is as succeeded
                    if (m_that->m_cancel)
                    {
                        success = true;
                    }
                    else
                    {
                        success = dynamic_cast<ICommand&>(*command).execute();
                    }
                }
            }
            catch (...) {}

            {
                QMutexLocker locker(&m_that->m_mutex);
                Q_ASSERT(m_that->m_remainingCommands > 0);

                --m_that->m_remainingCommands;

                // If not success, set success to false and emit
                if (!success)
                {
                    if (!m_that->m_cancel && m_that->m_listener && m_that->m_success)
                        m_that->m_listener->signalFailure();

                    m_that->m_success = false;
                }
                else // If success
                {
                    if (!m_that->m_cancel && m_that->m_listener)
                    {
                        if (m_that->m_remainingCommands == 0)
                            m_that->m_listener->signalSuccess(m_that->m_totalCommands);
                        else
                            m_that->m_listener->signalProgress(m_that->m_totalCommands -
                                                               m_that->m_remainingCommands,
                                                               m_that->m_totalCommands);
                    }
                }

                // Wakes all the waiting threads.
                m_that->m_processedCommand.wakeAll();
            }
        }
    }

    // The processor has finished its execution, sends finished signal
    // to quit and delete the thread
    // (see connects in constructor)
    emit finished();
}

}
