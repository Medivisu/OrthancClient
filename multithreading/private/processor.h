#ifndef PROCESSOR_H
#define PROCESSOR_H

#include <QObject>

QT_BEGIN_NAMESPACE
class QThread;
QT_END_NAMESPACE


namespace core
{

class ThreadedCommandProcessor;

/*!
 * \brief This class is not for user usage ! This is a "private class" used by
 * the ThreadedCommandProcessor, do not use it.
 *
 * This class represents a single thread of execution of the ThreadedCommandProcessor.
 */
class Processor : public QObject
{
        Q_OBJECT

    public:
        Processor(ThreadedCommandProcessor * that);
        ~Processor() = default;
        void start();

        bool isRunning() const;

    signals:
        void finished();

    private slots:
        void run();

    private:
        ThreadedCommandProcessor * m_that;
        QThread * m_thread;
        bool m_finished;


};

}

#endif // PROCESSOR_H
