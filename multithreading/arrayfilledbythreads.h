#ifndef ARRAYFILLEDBYTHREADS_H
#define ARRAYFILLEDBYTHREADS_H


#include "../idynamicobject.h"

#include <QMutex>
#include <QVector>

namespace core
{

class ArrayFilledByThreads
{
    public:
        class IFiller
        {
            public:
                virtual int getFillerSize() = 0;
                virtual IDynamicObject * getFillerItem(int index) = 0;
        };

        ArrayFilledByThreads(IFiller& filler);
        ~ArrayFilledByThreads();

        void reload();
        void invalidate();
        void setThreadCount(int threadCount);
        int threadCount() const;
        int getSize();
        IDynamicObject& getItem(int index);

    private:
        IFiller& m_filler;
        mutable QMutex m_mutex;
        QVector<IDynamicObject*> m_array;
        bool m_filled;
        int m_threadCount;

        class Command;

        void clear();
        void update();
};


}

#endif // ARRAYFILLEDBYTHREADS_H
