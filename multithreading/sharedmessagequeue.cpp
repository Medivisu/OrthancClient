#include "sharedmessagequeue.h"

#include <QDebug>
#include <QMutexLocker>
#include <QScopedPointer>

namespace core
{

SharedMessageQueue::SharedMessageQueue(int maxSize) :
    m_isFifo(true),
    m_maxSize(maxSize) {}

SharedMessageQueue::~SharedMessageQueue()
{
    qDeleteAll(m_queue);

    qDebug() << "Shared queue deleted...";
}

void SharedMessageQueue::enqueue(IDynamicObject * message)
{
    QMutexLocker locker(&m_mutex);

    if (m_maxSize != 0 && m_queue.size() > m_maxSize)
    {
        if (m_isFifo)
        {
            delete m_queue.front();
            m_queue.pop_front();
        }
        else
        {
            delete m_queue.back();
            m_queue.pop_back();
        }
    }

    if (m_isFifo)
        m_queue.push_back(message);
    else
        m_queue.push_front(message);

    m_elementAvailable.wakeOne();
}

IDynamicObject *SharedMessageQueue::dequeue(qint32 millisecondsTimeout)
{
    QMutexLocker locker(&m_mutex);

    while (m_queue.empty())
    {
        if (millisecondsTimeout == 0)
        {
            m_elementAvailable.wait(&m_mutex);
        }
        else
        {
            if (!m_elementAvailable.wait(&m_mutex, millisecondsTimeout))
                return nullptr;
        }
    }

    QScopedPointer<IDynamicObject> message(m_queue.front());
    m_queue.pop_front();

    if (m_queue.isEmpty())
        m_emptied.wakeAll();

    return message.take();
}

bool SharedMessageQueue::waitEmpty(qint32 millisecondsTimeout)
{
    QMutexLocker locker(&m_mutex);


    bool empty = true;

    // Wait for the queue to become empty
    while (!m_queue.empty())
    {
        if (millisecondsTimeout == 0)
            m_emptied.wait(&m_mutex);
        else
            empty = m_emptied.wait(&m_mutex, millisecondsTimeout);
    }

    return empty;
}

bool SharedMessageQueue::isFifoPolicy() const
{
    return m_isFifo;
}

bool SharedMessageQueue::isLifoPolicy() const
{
    return !m_isFifo;
}

void SharedMessageQueue::setFifoPolicy()
{
    QMutexLocker locker(&m_mutex);
    m_isFifo = true;
}

void SharedMessageQueue::setLifoPolicy()
{
    QMutexLocker locker(&m_mutex);
    m_isFifo = false;
}


}
