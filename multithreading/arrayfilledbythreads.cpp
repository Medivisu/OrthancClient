#include "arrayfilledbythreads.h"

#include "../commands/icommand.h"
#include "threadedcommandprocessor.h"

#include <QScopedPointer>
#include <QMutexLocker>

namespace core
{


class ArrayFilledByThreads::Command : public ICommand
{
        ArrayFilledByThreads& m_that;
        int m_index;

    public:
        Command(ArrayFilledByThreads& that, int index) :
            m_that(that), m_index(index) {}

        virtual bool execute() override
        {
            QScopedPointer<IDynamicObject> obj(m_that.m_filler.getFillerItem(m_index));
            if (obj.isNull())
            {
                return false;
            }
            else
            {
                QMutexLocker locker(&m_that.m_mutex);
                m_that.m_array[m_index] = obj.take();
                return true;
            }
        }


};

ArrayFilledByThreads::ArrayFilledByThreads(IFiller & filler) :
    m_filler(filler),
    m_filled(false),
    m_threadCount(4) {}

ArrayFilledByThreads::~ArrayFilledByThreads()
{
    clear();
}

void ArrayFilledByThreads::reload()
{
    clear();
    update();
}

void ArrayFilledByThreads::invalidate()
{
    clear();
}

/*!
 * \brief ArrayFilledByThreads::setThreadCount
 * \param threadCount
 */
void ArrayFilledByThreads::setThreadCount(int threadCount)
{
    if (threadCount >= 1)
        m_threadCount = threadCount;
}


/*!
 * \brief The thread count is 4 by default but you can modify it.
 * But it's not advised to have more than 4 threads.
 * \return
 */
int ArrayFilledByThreads::threadCount() const
{
    return m_threadCount;
}

int ArrayFilledByThreads::getSize()
{
    update();
    return m_array.size();
}

IDynamicObject &ArrayFilledByThreads::getItem(int index)
{
    if (index >= getSize())
        throw std::invalid_argument("The index is out of range");

    return *m_array.at(index);
}



// PRIVATE METHODS

void ArrayFilledByThreads::clear()
{
    for (int i = 0; i < m_array.size(); ++i)
    {
        if (m_array[i])
            delete m_array[i];
    }

    m_array.clear();
    m_filled = false;
}

void ArrayFilledByThreads::update()
{
    if (!m_filled)
    {
        m_array.resize(m_filler.getFillerSize());

        ThreadedCommandProcessor processor(m_threadCount);
        for (int i = 0; i < m_array.size(); ++i)
            processor.post(new Command(*this, i));

        processor.join();
        m_filled = true;
    }
}

}
