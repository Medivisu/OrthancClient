#ifndef PATIENT_H
#define PATIENT_H

#include <QDate>
#include <QJsonDocument>
#include <QList>
#include <QString>

#include "multithreading/arrayfilledbythreads.h"

#include "iorthancresource.h"
#include "orthancconnection.h"
#include "orthancstudy.h"

namespace orthanc_client
{

/*!
 * \brief The patient class in the OrthancClient namespace corresponds to a
 * patient stored in an Orthanc Server.
 */
class OrthancPatient : public IOrthancResource,
                       private core::ArrayFilledByThreads::IFiller
{

    public:

        // Main tags
        static const char PATIENT_NAME[];
        static const char PATIENT_ID[];

        OrthancPatient(orthanc_client::OrthancConnection& connection,
                const QString& id);

        int getStudyCount();
        OrthancStudy& getStudy(int index);
        OrthancStudy& getStudyByID(const QString & id);


        void deleteResource() override;

        void downloadAsZip(const QString& path, QString zipName);
        void downloadAsZip(const QString& path);

        QString anonymize(bool reload);
        QString anonymize(const QVariantMap & replace,
                          const QStringList & keep,
                          const QStringList & remove,
                          bool keepPrivateTags);

        bool isAnonymized() const;
        bool isAnonymized(QString& parentID) const;


        void reload();

    private:
        core::ArrayFilledByThreads m_studies;
        bool m_anonymized;

        // Private methods
        void readPatient();
        virtual int getFillerSize();
        virtual core::IDynamicObject * getFillerItem(int index);
};

} // namespace OrthancClient

#endif // PATIENT_H
