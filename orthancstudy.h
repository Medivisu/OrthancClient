#ifndef STUDY_H
#define STUDY_H

#include <QDate>
#include <QString>
#include <QTime>

#include "orthancconnection.h"
#include "orthancserie.h"

#include "iorthancresource.h"

namespace orthanc_client
{

/*!
 * \brief The study class in the OrthancClient namespace corresponds to a
 * study stored in an Orthanc Server.
 */
class OrthancStudy : public IOrthancResource,
              private core::ArrayFilledByThreads::IFiller
{
    public:

        // The main tags
        static const char ACCESSION_NUMBER[];
        static const char STUDY_DATE[];
        static const char STUDY_DESCRIPTION[];
        static const char STUDY_ID[];
        static const char STUDY_INSTANCE_UID[];
        static const char STUDY_TIME[];

        OrthancStudy(orthanc_client::OrthancConnection& connection,
              const QString& id);

        int getSerieCount();
        OrthancSerie& getSerie(int index);
        OrthancSerie &getSerieByID(const QString & id);

        QString anonymize(bool reload);

        void downloadAsZip(const QString & path, QString zipName);
        void downloadAsZip(const QString & path);

        void deleteResource() override;

        QString anonymize(const QVariantMap & replace,
                          const QStringList & keep,
                          const QStringList & remove,
                          bool keepPrivateTags);

    private:
        core::ArrayFilledByThreads m_series;

        // Private methods
        void readStudy();
        virtual int getFillerSize();
        virtual core::IDynamicObject * getFillerItem(int index);
};

} // namespace OrthancClient

#endif // STUDY_H
