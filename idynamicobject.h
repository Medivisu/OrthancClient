#ifndef IDYNAMICOBJECT
#define IDYNAMICOBJECT

#include "boost/noncopyable.hpp"
#include <QtGlobal>

namespace core
{

/**
 * This class should be the ancestor to any class whose type is
 * determined at the runtime, and that can be dynamically allocated.
 * Being a child of IDynamicObject only implies the existence of a
 * virtual destructor.
 **/
class IDynamicObject : public boost::noncopyable
{
    public:
        virtual ~IDynamicObject() {}
};

}

#endif // IDYNAMICOBJECT

