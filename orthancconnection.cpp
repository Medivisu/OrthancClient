#include "orthancconnection.h"

#include <QDebug>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonValue>

#include "orthancclientexception.h"
#include "orthancpatient.h"
#include "toolbox.h"

namespace orthanc_client
{

/*!
 * \brief Creates a new connection to an Orthanc server.
 *
 * The validity of the URL is left to the user (validity = right url for the server).
 *
 * \param orthancUrl the url of an Orthanc server.
 */
OrthancConnection::OrthancConnection(const QString & orthancUrl) : m_patients(*this)
{
    m_url.setUrl(orthancUrl);
    readPatients();
}

/*!
 * \brief Creates a new connection to an Orthanc server.
 *
 * The validity of the URL is left to the user (validity = right url for the server).
 * The credentials are not checked at the creation.
 * \param orthancUrl the url of an Orthanc server.
 * \param username the username that allows you to connect to the server.
 * \param password the password that allows you to connect to the server.s
 */
OrthancConnection::OrthancConnection(const QString & orthancUrl,
                                     const QString & username,
                                     const QString & password) : m_patients(*this)
{
    m_url.setUrl(orthancUrl);
    m_client.setCredentials(username, password);
    readPatients();
}

/*!
 * \brief Returns the number of patients.
 * \return the number of patients stored on the server.
 */
int OrthancConnection::getPatientCount()
{
    return m_patients.getSize();
}

/*!
 * \brief Returns the patient at the given index.
 * \param index the index of the patient.
 * \return the patient at the index.
 * \throws OrthancClientException if the index is out of range.
 */
orthanc_client::OrthancPatient& OrthancConnection::getPatient(int index)
{
    return dynamic_cast<orthanc_client::OrthancPatient&>(m_patients.getItem(index));
}

/*!
 * \brief Returns the patient by ID.
 *
 * This method is longer than getPatient(int index). It searches all the patients
 * until it finds the right ID. If the ID has not been found, an exception is
 * thrown.
 * \param id the id.
 * \return the patient.
 * \throw OrthancClientException if no patient was found.
 */
OrthancPatient & OrthancConnection::getPatientByOrthancID(const QString & id)
{
    int i = 0;
    while (i < getPatientCount() && getPatient(i).getOrthancID() != id)
        ++i;

    if (i >= getPatientCount())
        throw OrthancClientException("No patient found : no patient with the id : " + id);

    return getPatient(i);
}

/*!
 * \brief Reload the informations.
 */
void OrthancConnection::reload()
{
    readPatients();
    m_patients.invalidate();
}

/*!
 * \brief Delete a patient from the server.
 *
 * This method is equivalent to getPatient(index).deletePatient().
 * See the documentation of deletePatient for more information.
 *
 * \param index the index of the patient.
 */
void OrthancConnection::deletePatient(int index)
{
    getPatient(index).deleteResource();
}

/*!
 * \brief Store a new instance of a dicom file on the server.
 *
 * If the file provided by the binary data is not a dicom file, the file will
 * probably not be stored on the server but you might not be notified.
 *
 * \param dicom the binary data of the dicom.
 * \throw OrthancClientException if the request fails.
 */
void OrthancConnection::store(const QByteArray & dicom)
{
    if (dicom.isEmpty() || dicom.isNull())
        throw orthanc_client::OrthancClientException("File empty or not dicom !");

    m_client.setMethod(http::HttpMethod::POST);
    m_client.setUrl(m_url.toString() + "/instances");
    m_client.setPostData(dicom);

    QByteArray replyContent;

    if (!m_client.apply(replyContent))
        throw orthanc_client::OrthancClientException(m_client.lastError());

    reload();
}

/*!
 * \brief Store a new dicom file on the server.
 *
 * If the file provided by dicomPath is not a dicom file, the file will
 * probably not be stored on the server but you might not be notified.
 *
 * \param dicomPath the path of the file.
 * \return false if the file does not exist or impossible to read.
 * \throw OrthancClientException if the file is not readable or has not
 * been found.
 */
void OrthancConnection::storeFile(const QString & dicomPath)
{
    if (!toolbox::isWellFormedDicom(dicomPath))
        qWarning() << "Warning : The dicom file : " << dicomPath << " is not well formed.";

    QByteArray dicom;
    if (!toolbox::readFile(dicom, dicomPath))
        throw orthanc_client::OrthancClientException("File not found or not readable !");

    store(dicom);
}

//================ PRIVATE METHODS ================//

// Read the patients id from the server.
void OrthancConnection::readPatients()
{
    m_client.setMethod(http::HttpMethod::GET);
    m_client.setUrl(m_url.toString() + "/patients");

    if (!m_client.apply(m_content))
        throw orthanc_client::OrthancClientException(m_client.lastError());
}

// Get the patient's count.
int OrthancConnection::getFillerSize()
{
    return m_content.array().size();
}

// Returns a new patient.
core::IDynamicObject * OrthancConnection::getFillerItem(int index)
{
    QString id = m_content.array().at(index).toString();
    return new orthanc_client::OrthancPatient(*this, id);
}

} // namespace OrthancClient
