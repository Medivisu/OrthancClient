# IMPORTANT

Some classes have been changed recently with the same working classes from another project but with another implementation, this should work but has not been tested. Report if a bug appears.

# Orthanc Client

The Orthanc Client allows you to communicate with an Orthanc Server.

This repository has been heavily inspired by https://bitbucket.org/sjodogne/orthanc-client (can be considered as a fork).
I took a lot of the code and architecture from the original repository, but I
have rewritten it with QtCore Framework (more high-level programming) to make it
easier to understand and debug
and to facilitate the integration in the future Medivisu Application.
This client has all the features (almost, 3D is missing)
of the original orthanc-client, but some more are implemented (like anonymization, download zip archive, and so on).

I am trying to keep it well documented. Don't hesitate to post an issue on the
repository.

## What the client allows you to do :

* Create a custom connection with an URL, username, password.
* Get all the patients / studies / series / instances informations (tags, and others).
* Get resources by index, or ID. ID is obviously longer than index.
    - You can get a patient by his ID. 
    - You can get a study by ID, but only
    if you already have the patient of this study. You can get a serie by
    id but only if you already have the study of this serie, same for the instance.
* Store a dicom file on the server.
* Download the dicom file of an instance stored on the server and write it on disk.
* Download the image file (PNG) of an instance stored on the server and write it on disk.
    - The image file can have multiple image format. Working for now : preview and uint8.
* Delete a patient, study, serie, instance stored on the server.
* Simple anonymization of a patient, study, serie.
    - The anonymization copies the resource and anonymizes it. It returns you
    the new ID for this resource. You can chose if you want to reload the data
    or not.
* Complex anonymization for a patient, study, serie.
	- This anonymization is the same that the simple one, but you can
	specify the tags that you replace, remove, keep, and chose if you
	keep the private tags.
* Simple and complex anonymization for an instance.
	- For an instance, the anonymization returns the data of a new modified dicom file.
	You can then write it on the disk or use it in your program.
* Download a patient as ZIP, that means downloading all of his studies / series / instances of this patient.
* Download a study as ZIP, that means downloading all of the series / instances of this study.
* Download a serie as ZIP, that means downloading all of the instance of this serie.

