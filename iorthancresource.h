#ifndef IDICOMOBJECT
#define IDICOMOBJECT

#include "orthancconnection.h"

#include <QDebug>
#include <QJsonDocument>
#include <QJsonValue>
#include <QJsonObject>
#include <QJsonArray>
#include <QVariantMap>
#include <QStringList>
#include <QString>
#include "idynamicobject.h"
#include "orthancclientexception.h"
#include "toolbox.h"

namespace orthanc_client
{

/*!
 * \brief Superclass for patient, serie, study, instance provided by orthanc.
 */
class IOrthancResource : public core::IDynamicObject
{

    public:

        virtual ~IOrthancResource() = default;
        virtual void deleteResource() = 0;


        /*!
         * \brief Returns the value of one of the main dicom tags.
         * \param tag the tag.
         * \param defaultValue the default value if the tag does not exists.
         * \return the value of one of the main dicom tags, or the default value if the tag
         * does not exists.
         */
        QString getMainDicomTag(const QString& tag, const QString& defaultValue = "") const
        {
            QJsonObject o = m_content.object()["MainDicomTags"].toObject();
            if (o.contains(tag))
                return o[tag].toString();
            else
                return defaultValue;
        }

        QString getTag(const QString& tag, const QString& defaultValue = "") const
        {
            QJsonObject o = m_content.object();
            if (o.contains(tag))
                return o[tag].toString();
            else
                return defaultValue;
        }

        /*!
         * \brief Returns a QByteArray of the JSON provided by Orthanc.
         * \return a QByteArray of the JSON provided by Orthanc.
         */
        QByteArray toJSON() const
        {
            return m_content.toJson();
        }

        /*!
         * \brief Returns the Orthanc ID.
         * \return the Orthanc ID.
         */
        const QString& getOrthancID() const
        {
            return m_id;
        }

    protected:
        orthanc_client::OrthancConnection& m_connection;
        QString m_id;
        QJsonDocument m_content;

        // Protected methods.
        IOrthancResource(orthanc_client::OrthancConnection& connection,
                         const QString& id) :
            m_connection(connection),
            m_id(id) {}


        /*!
         * \brief Download the resource type as an archive, see documentation
         * of the public methods.
         * \param path the directory path.
         * \param zipName the archive name.
         * \param ressourceType the resource type.
         */
        void downloadAsZip(const QString& path, QString zipName, const QString& resourceType)
        {
            http::HttpClient client = m_connection.httpClient();
            client.setUrl(m_connection.url().toString() + resourceType + m_id + "/archive");
            client.setMethod(http::HttpMethod::GET);
            QByteArray content;
            if (!client.apply(content))
                throw OrthancClientException(client.lastError());

            // Add .zip extension if not in the zipName
            if (!zipName.endsWith(".zip"))
                zipName.append(".zip");

            if (!toolbox::writeFile(content, path, zipName))
                throw OrthancClientException("Cannot write file to : " + path);
        }

        /*!
         * \brief Delete a resource from the server. The content that
         * have been already get from the server will be invalidated.
         * \param resourceType the resource type.
         */
        void deleteResource(const QString& resourceType)
        {
            http::HttpClient client = m_connection.httpClient();
            client.setUrl(m_connection.url().toString() + resourceType + m_id);
            client.setMethod(http::HttpMethod::DELETE);
            QByteArray dummyContent;
            if (!client.apply(dummyContent))
                throw OrthancClientException(client.lastError());

            m_connection.reload();
        }

        /*!
         * \brief Anonymize a resource stored on the Orthanc server.
         * \param resourceType the resource type.
         * \param reload reload the connection if true. If false, the reload is
         * leaved to the caller.
         * \return the new resource's Orthanc ID.
         */
        QString anonymize(const QString& resourceType, bool reload)
        {
            http::HttpClient client = m_connection.httpClient();
            client.setUrl(m_connection.url().toString() + resourceType + m_id + "/anonymize");
            client.setMethod(http::HttpMethod::POST);
            client.setPostData("{}"); // Fully anonymize.
            QJsonDocument doc;
            if (!client.apply(doc))
                throw OrthancClientException(client.lastError());

            if (reload)
                m_connection.reload(); // Reload the connection.

            return doc.object()["ID"].toString(); // return the new ID.
        }

        QString anonymize(const QVariantMap & replace,
                          const QStringList & keep,
                          const QStringList & remove,
                          bool keepPrivateTags,
                          const QString& resourceType)
        {
            QJsonObject object;
            if (!replace.isEmpty())
                object.insert("Replace", QJsonObject::fromVariantMap(replace));

            if (!keep.isEmpty())
                object.insert("Keep", QJsonArray::fromStringList(keep));

            if (!remove.isEmpty())
                object.insert("Remove", QJsonArray::fromStringList(remove));


            if (keepPrivateTags)
                object.insert("KeepPrivateTags", QJsonValue());

            QJsonDocument postData(object);
            http::HttpClient client;
            client.setUrl(m_connection.url().toString() + resourceType + m_id + "/anonymize");
            client.setMethod(http::HttpMethod::POST);

            client.setPostData(postData.toJson());
            QJsonDocument result;
            if (!client.apply(result))
                throw OrthancClientException(client.lastError());

            m_connection.reload();

            return result.object()["ID"].toString();
        }
};

}

#endif // IDICOMOBJECT

