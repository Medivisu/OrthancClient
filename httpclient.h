#ifndef HTTPCLIENT_H
#define HTTPCLIENT_H

#include <QObject>
#include <QUrl>

class QNetworkAccessManager;
class QNetworkReply;

namespace http
{

/*!
 * \brief The HttpMethod enum represents the four principal
 * HTTP methods.
 */
enum class HttpMethod
{
    GET,
    POST,
    PUT,
    DELETE
};

/*!
 * \brief HttpClient is a very basic HTTP client built with Qt.
 *
 * The HTTP client allows you to do the four principal HTTP methods
 * GET, POST, PUT and DELETE.
 *
 */
class HttpClient : public QObject
{
        Q_OBJECT

    public:
        explicit HttpClient(QObject * parent = 0);
        HttpClient(const HttpClient& client);
        ~HttpClient();

        void setUrl(const QString& url, QMap<QString, QString>& parameters);
        void setUrl(const QString& url);
        void setUrl(const QUrl &url, QMap<QString, QString>& parameters);
        void setUrl(const QUrl& url);

        void setPostData(const QByteArray& postData);
        void setMethod(HttpMethod httpMethod);

        void setCredentials(const QString& username, const QString & password);

        bool apply(QJsonDocument& json);
        bool apply(QByteArray& replyContent);

        const QString& lastError() const;

    signals:
        void uploadProgress(qint64 bytesReceived, qint64 bytesTotal);
        void downloadProgress(qint64 bytesReceived, qint64 bytesTotal);


    private:
        QNetworkAccessManager * m_manager;
        QUrl m_url;
        HttpMethod m_httpMethod;
        QByteArray m_postData;
        QString m_username;
        QString m_password;
        QString m_lastError;

        bool doGet(QByteArray & replyContent);
        bool doPost(QByteArray & replyContent);
        bool doPut(QByteArray & replyContent);
        bool doDelete(QByteArray & replyContent);

        void connectReply(QNetworkReply * reply);
        bool fillReplyContent(QNetworkReply * reply, QByteArray & replyContent);
        bool isSuccess(QNetworkReply * reply);
        void waitForFinish(QNetworkReply * reply);
};

} // namespace http

#endif // HTTPCLIENT_H
