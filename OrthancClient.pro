#-------------------------------------------------
#
# Project created by QtCreator 2015-07-24T09:38:36
#
#-------------------------------------------------

QT       += core network

QMAKE_CXXFLAGS = -std=c++11 -Wno-unused-parameter

TARGET = OrthancClient
#CONFIG   += console
CONFIG   -= app_bundle

TEMPLATE = app

SOURCES += main.cpp \
    multithreading/sharedmessagequeue.cpp \
    multithreading/threadedcommandprocessor.cpp \
    multithreading/arrayfilledbythreads.cpp \
    multithreading/private/processor.cpp \
    httpclient.cpp \
    toolbox.cpp \
    orthancconnection.cpp \
    orthancpatient.cpp \
    orthancinstance.cpp \
    orthancstudy.cpp \
    orthancserie.cpp

HEADERS += \
    commands/icommand.h \
    multithreading/sharedmessagequeue.h \
    multithreading/arrayfilledbythreads.h \
    multithreading/private/processor.h \
    multithreading/threadedcommandprocessor.h \
    httpclient.h \
    orthancconnection.h \
    orthancclientexception.h \
    idynamicobject.h \
    toolbox.h \
    iorthancresource.h \
    orthancinstance.h \
    orthancpatient.h \
    orthancserie.h \
    orthancstudy.h \
    enumerations.h \



