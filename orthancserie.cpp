#include "orthancserie.h"

#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>
#include <QSet>

#include "orthancclientexception.h"
#include "commands/icommand.h"
#include "multithreading/threadedcommandprocessor.h"
#include "toolbox.h"

namespace orthanc_client
{

const char OrthancSerie::MANUFACTURER[]        = "Manufacturer";
const char OrthancSerie::MODALITY[]            = "Modality";
const char OrthancSerie::SERIES_DATE[]         = "SeriesDate";
const char OrthancSerie::SERIES_DESCRIPTION[]  = "SeriesDescription";
const char OrthancSerie::SERIES_INSTANCE_UID[] = "SeriesInstanceUID";
const char OrthancSerie::SERIES_NUMBER[]       = "SeriesNumber";
const char OrthancSerie::SERIES_TIME[]         = "SeriesTime";

namespace
{

class SliceLocator
{
        float m_normal[3];

    public:
        SliceLocator(OrthancInstance& instance)
        {
            QVector<float> cosines;
            instance.splitIntoVectorOfFloats(cosines, "ImageOrientationPatient");

            if (cosines.size() != 6)
                throw OrthancClientException("Bad file format");


            // Cross product
            m_normal[0] = cosines.at(1) * cosines.at(5) - cosines.at(2) * cosines.at(4);
            m_normal[1] = cosines.at(2) * cosines.at(3) - cosines.at(0) * cosines.at(5);
            m_normal[2] = cosines.at(0) * cosines.at(4) - cosines.at(1) * cosines.at(3);
        }


        float computeSliceLocation(OrthancInstance& instance) const
        {
            QVector<float> ipp;
            instance.splitIntoVectorOfFloats(ipp, "ImagePositionPatient");  // 0020-0032
            if (ipp.size() != 3)
                throw OrthancClientException("Bad file format");

            float dist = 0;

            // Dot product
            for (int i = 0; i < 3; i++)
                dist += m_normal[i] * ipp.at(i);

            return dist;
        }

};


class ImageDownloadCommand : public core::ICommand
{


    private:
        OrthancInstance& m_instance;
        QPixelFormat m_format;
        ImageExtractionMode m_mode;
        void * m_target;
        int m_lineStride;

    public:
        ImageDownloadCommand(OrthancInstance& instance,
                             QPixelFormat format,
                             ImageExtractionMode mode,
                             void * target,
                             int lineStride) :
            m_instance(instance),
            m_format(format),
            m_mode(mode),
            m_target(target),
            m_lineStride(lineStride)
        {
            m_instance.setImageExtractionMode(m_mode);
        }

        bool execute()
        {

            int width = m_instance.getHeight();
            for (int i = 0; i < m_instance.getHeight(); ++i)
            {

                quint8 * p = static_cast<quint8*> (m_target) + (i * m_lineStride);

                QPixelFormat format = m_instance.pixelFormat();
                if (format == m_format)
                {
                    memcpy(p, ((void *) m_instance.getImageDataLine(i).data()), m_format.bitsPerPixel() / 8 );
                }
                else if ((format.colorModel() == QPixelFormat::Grayscale &&
                          format.typeInterpretation() == QPixelFormat::UnsignedByte) ||
                         (format.colorModel() == QPixelFormat::RGB &&
                          format.typeInterpretation() == QPixelFormat::UnsignedInteger))
                {

                    char * s =  (m_instance.getImageDataLine(i).data());
                    for (int x = 0; x < width; ++x, ++s, p += 3)
                    {
                        p[0] = p[1] = p[2] = *s;
                    }

                }
                else
                {
                    throw OrthancClientException("Format not implemented");
                }
            }

            m_instance.discardImage();
            return true;
        }
};

class ProgressToFloatListener : public core::ThreadedCommandProcessor::IListener
{
    private:
        float& m_target;

    public:
        ProgressToFloatListener(float& target) : m_target(target)
        {
        }

        virtual void signalProgress(int current,
                                    int total)
        {
            if (total == 0)
            {
                m_target = 0;
            }
            else
            {
                m_target = static_cast<float>(current) / static_cast<float>(total);
            }
        }

        virtual void signalSuccess(int total)
        {
            m_target = 1;
        }

        virtual void signalFailure()
        {
            m_target = 0;
        }

        virtual void signalCancel()
        {
            m_target = 0;
        }
};

}

/*!
 * \brief Creates a new serie provided by Orthanc.
 * \param connection the Orthanc connection.
 * \param id the Orthanc id of the serie.
 */
OrthancSerie::OrthancSerie(orthanc_client::OrthancConnection & connection,
                           const QString & id) :
    IOrthancResource(connection, id),
    m_instances(*this)

{
    m_instances.setThreadCount(m_connection.threadCount());
    readSerie();
}

/*!
 * \brief Get the instance count.
 * \return the instance count.
 */
int OrthancSerie::getInstanceCount()
{
    return m_instances.getSize();
}

/*!
 * \brief Get the instance at the given index.
 * \param index the index.
 * \return the instance at the given index.
 */
OrthancInstance & OrthancSerie::getInstance(int index)
{
    return dynamic_cast<OrthancInstance&> (m_instances.getItem(index));
}

/*!
 * \brief Returns the instance by ID.
 *
 * This method is longer than getInstance(int index). It searches all the instances
 * until it finds the right ID. If the ID has not been found, an exception is
 * thrown.
 * \param id the id.
 * \return the instance.
 * \throw OrthancClientException if no instance was found.
 */
OrthancInstance & OrthancSerie::getInstanceByID(const QString& id)
{
    int i = 0;
    while (i < getInstanceCount() && getInstance(i).getOrthancID() != id)
        ++i;

    if (i >= getInstanceCount())
        throw OrthancClientException("No instance found : no instance with the id : " + id);

    return getInstance(i);
}


/*!
 * \brief Copy the serie and anonymize it, returns the new serie orthanc ID.
 *
 * The series will not be updated until you reload the connection yourself.
 *
 * \return the new serie Orthanc ID.
 */
QString OrthancSerie::anonymize(bool reload)
{
    return IOrthancResource::anonymize("/series/", reload);
}

/*!
 * \brief Anonymize the serie.
 *
 * <ul>
 * <li> You can specify the tags that you want to replace in a map in the form [tag-name, newValue]. </li>
 * <li> You can specify the tags that you want to keep in a list. </li>
 * <li> You can specify the tags that you want to remove in a list. </li>
 * </ul>
 * \param replace the map of tags-value that you want to replace.
 * \param keep the list of tags that you want to keep.
 * \param remove the list of tags that you want to remove.
 * \param keepPrivateTags true if you want to keep the private tags, false otherwise.
 * \return the new ID of the modified serie.
 */
QString OrthancSerie::anonymize(const QVariantMap & replace,
                                const QStringList & keep,
                                const QStringList & remove,
                                bool keepPrivateTags)
{
    return IOrthancResource::anonymize(replace, keep,
                                       remove, keepPrivateTags,
                                       "/series/");
}

/*!
 * \brief Deletes the serie from the Orthanc Server.
 * The content you already get will be invalidated.
 *
 * \throw OrthancClientException if the Http Client fails the delete request.
 */
void OrthancSerie::deleteResource()
{
    IOrthancResource::deleteResource("/series/");
}


/*!
 * \brief Download an archive of the serie with all the instances.
 * \param path the directory path where the archive will be saved.
 * \param zipName the archive name.
 * \throw OrthancClientException if the download fails, or if you cannot write
 * to the specified directory / path.
 */
void OrthancSerie::downloadAsZip(const QString & path, QString zipName)
{
    IOrthancResource::downloadAsZip(path, zipName, "/series/");
}

/*!
 * \brief Download an archive of the serie with all the instances.
 * This method will give the Orthanc ID as the name of the archive.
 * If you want to specify your own name, use the overloaded function :
 * downloadAsZip(const QString& path, const QString& zipName)
 * \param path the directory path where the archive will be saved.
 * \throw OrthancClientException if the download fails, or if you cannot write
 * to the specified directory / path.
 */
void OrthancSerie::downloadAsZip(const QString & path)
{
    downloadAsZip(path, m_id + ".zip");
}


// ========== PRIVATE METHOD ========== //

void OrthancSerie::readSerie()
{
    http::HttpClient client = m_connection.httpClient();
    client.setMethod(http::HttpMethod::GET);
    client.setUrl(m_connection.url().toString() + "/series/" + m_id);

    if (!client.apply(m_content))
        throw orthanc_client::OrthancClientException(client.lastError());
}

int OrthancSerie::getFillerSize()
{
    return m_content.object()["Instances"].toArray().size();
}

core::IDynamicObject * OrthancSerie::getFillerItem(int index)
{
    QString id = m_content.object()["Instances"].toArray()[index].toString();
    return new OrthancInstance(m_connection, id);
}


} // namespace OrthancClient



