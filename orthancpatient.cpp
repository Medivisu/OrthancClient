#include "orthancpatient.h"

#include <QDebug>
#include <QDir>
#include <QJsonArray>
#include <QJsonObject>

#include "toolbox.h"
#include "httpclient.h"
#include "orthancclientexception.h"


namespace orthanc_client
{

const char OrthancPatient::PATIENT_ID[]   = "PatientID";
const char OrthancPatient::PATIENT_NAME[] = "PatientName";

/*!
 * \brief Creates a new patient instance.
 * \param connection the Orthanc connection.
 * \param id the orthanc id of the patient.
 * \throw OrthancClientException if the Http Client fails the GET request
 * to read the patient's informations.
 */
OrthancPatient::OrthancPatient(orthanc_client::OrthancConnection & connection,
                               const QString & id) :
    IOrthancResource(connection, id),
    m_studies(*this)
{
    m_studies.setThreadCount(connection.threadCount());
    readPatient();
}

/*!
 * \brief Returns the number of studies the patient has been the subject to.
 * \return the number of studies.
 */
int OrthancPatient::getStudyCount()
{
    return m_studies.getSize();
}

/*!
 * \brief Returns the study with the given index.
 * \param index the index of the study.
 * \return the study with the given index.
 */
OrthancStudy & OrthancPatient::getStudy(int index)
{
    return static_cast<orthanc_client::OrthancStudy&> (m_studies.getItem(index));
}

/*!
 * \brief Returns the study by ID.
 *
 * This method is longer than getStudy(int index). It searches all the studies
 * until it finds the right ID. If the ID has not been found, an exception is
 * thrown.
 * \param id the id.
 * \return the study.
 * \throw OrthancClientException if no study was found.
 */
OrthancStudy& OrthancPatient::getStudyByID(const QString& id)
{
    int i = 0;
    while (i < getStudyCount() && getStudy(i).getOrthancID() != id)
        ++i;

    if (i >= getStudyCount())
        throw OrthancClientException("No study found : no study with the id : " + id);

    return getStudy(i);
}

/*!
 * \brief Deletes the patient from the Orthanc Server. The patient, all his
 * studies, series and instances will be deleted with him. Use carefully.
 *
 * The content you have already get will be invalidated.
 *
 * \throw OrthancClientException if the Http Client fails the delete request.
 */
void OrthancPatient::deleteResource()
{
    IOrthancResource::deleteResource("/patients/");
}

/*!
 * \brief Download an archive of the patient with all the studies / series / instances.
 * \param path the directory path where the archive will be saved.
 * \param zipName the archive name.
 * \throw OrthancClientException if the download fails, or if you cannot write
 * to the specified directory / path.
 */
void OrthancPatient::downloadAsZip(const QString & path, QString zipName)
{
    IOrthancResource::downloadAsZip(path, zipName, "/patients/");
}

/*!
 * \brief Download an archive of the patient with all the studies / series / instances.
 * This method will give the Orthanc ID as the name of the archive.
 * If you want to specify your own name, use the overloaded function :
 * downloadAsZip(const QString& path, const QString& zipName)
 * \param path the directory path where the archive will be saved.
 * \throw OrthancClientException if the download fails, or if you cannot write
 * to the specified directory / path.
 */
void OrthancPatient::downloadAsZip(const QString & path)
{
    downloadAsZip(path, m_id + ".zip");
}

/*!
 * \brief Copy the patient and anonymize it, returns the new patient orthanc ID.
 *
 * If reload is true, the patients will be invalidated.
 * \param reload true if the connection has to be reloaded, false otherwise.
 * \return the new patient Orthanc ID.
 */
QString OrthancPatient::anonymize(bool reload)
{
    return IOrthancResource::anonymize("/patients/", reload);
}

QString OrthancPatient::anonymize(const QVariantMap & replace,
                                  const QStringList & keep,
                                  const QStringList & remove,
                                  bool keepPrivateTags)
{
    return IOrthancResource::anonymize(replace, keep,
                                       remove, keepPrivateTags,
                                       "/patients/");
}

/*!
 * \brief Returns true if the patient is an anonymized patient.
 * \return true if the patient is an anonymized patient.
 */
bool OrthancPatient::isAnonymized() const
{
    return m_anonymized;
}

/*!
 * \brief Returns true if the patient is an anonymized patient.
 *
 * If the patient is anonymized, set the parentID to the parent patient
 * orthanc ID that created this patient.
 *
 * \return true if the patient is an anonymized patient.
 */
bool OrthancPatient::isAnonymized(QString & parentID) const
{
    parentID.clear();
    if (m_anonymized)
        parentID = getTag("AnonymizedFrom");

    return m_anonymized;
}

/*!
 * \brief Reload the patient data.
 */
void OrthancPatient::reload()
{
    m_studies.reload();
}

//================ PRIVATE METHODS ================//

// Read the patient data from the server.
void OrthancPatient::readPatient()
{
    http::HttpClient client = m_connection.httpClient();
    client.setMethod(http::HttpMethod::GET);
    client.setUrl(m_connection.url().toString() + "/patients/" + m_id);

    if (!client.apply(m_content))
        throw orthanc_client::OrthancClientException(client.lastError());

    m_anonymized = !m_content.object()["AnonymizedFrom"].toString().isEmpty();
}

// Returns the number of studies
int OrthancPatient::getFillerSize()
{
    return m_content.object()["Studies"].toArray().size();
}

// Return a new study.
core::IDynamicObject * OrthancPatient::getFillerItem(int index)
{
    QString id = m_content.object()["Studies"].toArray()[index].toString();
    return new OrthancStudy(m_connection, id);
}

} // namespace OrthancClient
