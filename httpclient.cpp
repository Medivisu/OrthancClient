#include "httpclient.h"

#include <QAuthenticator>
#include <QEventLoop>
#include <QJsonDocument>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QUrlQuery>

namespace http
{

/*!
 * \brief Creates a new Http Client.
 * \param parent the parent object.
 */
HttpClient::HttpClient(QObject * parent) : QObject(parent)
{
    m_manager = new QNetworkAccessManager(this);

    // Not tested !
    connect(m_manager, &QNetworkAccessManager::authenticationRequired, this,
            [this] (QNetworkReply * reply, QAuthenticator * authentificator)
    {
        authentificator->setUser(m_username);
        authentificator->setPassword(m_password);
    });
}

/*!
 * \brief Copies an existing Http Client.
 * \param client the client to copy.
 */
HttpClient::HttpClient(const HttpClient & client) : QObject(client.parent())
{
    m_manager  = new QNetworkAccessManager(this);
    m_url      = client.m_url;
    m_username = client.m_username;
    m_password = client.m_password;
}

/*!
 * \brief Destroy the Http Client.
 */
HttpClient::~HttpClient()
{
    m_manager->deleteLater();
}

/*!
 * \brief Sets the URL and the URL parameters.
 *
 * <p>
 * <ul>
 * <li> The url looks like http://www.mysite.com/. </li>
 * <li> The parameters map will be transleted into
 *      URL parameters. {{"key", "value"}, {"key2", "value2"}}
 *      will be translated into ?key=value&key2=value2. </li>
 * <li> The final URL will be http://www.mysite.com?key=vlaue&key2=value2 </li>
 * </ul>
 * </p>
 *
 *
 * \param url the URL.
 * \param parameters the URL parameters.
 */
void HttpClient::setUrl(const QString & url, QMap<QString, QString> & parameters)
{
    setUrl(QUrl(url), parameters);
}

/*!
 * \brief Sets the URL to url.
 * \param url the new URL.
 */
void HttpClient::setUrl(const QString & url)
{
    setUrl(QUrl(url));
}

/*!
 * \brief Sets the URL and the URL parameters.
 *
 * This is the same method than
 * setUrl(const QString & url, QMap<QString, QString & parameters)
 * but the URL is given with a QUrl object.
 *
 * \param url the URL.
 * \param parameters the URL parameters.
 */
void HttpClient::setUrl(const QUrl & url, QMap<QString, QString> & parameters)
{
    QUrlQuery query(url);
    for (const auto& key : parameters.keys())
        query.addQueryItem(key, parameters[key]);

    m_url = url;
    m_url.setQuery(query);
}

/*!
 * \brief Sets the URL to url.
 * \param url the new url.
 */
void HttpClient::setUrl(const QUrl & url)
{
    m_url = url;
}

/*!
 * \brief Sets the post data.
 * \param postData the new post data.
 */
void HttpClient::setPostData(const QByteArray & postData)
{
    m_postData = postData;
}

/*!
 * \brief Sets the HTTP method.
 * \param httpMethod the new HTTP method.
 */
void HttpClient::setMethod(HttpMethod httpMethod)
{
    m_httpMethod = httpMethod;
}

/*!
 * \brief Sets the credentials.
 * \param username the username.
 * \param password the password.
 */
void HttpClient::setCredentials(const QString& username, const QString& password)
{
    m_username = username;
    m_password = password;
}

/*!
 * \brief Executes the HTTP request.
 *
 * The JSON document will be filled if the request succeeds.
 *
 * \param json the json document.
 * \return true if the request succeeds, false otherwise.
 */
bool HttpClient::apply(QJsonDocument & json)
{
    QByteArray content;
    bool success = apply(content);

    if (success)
        json = QJsonDocument::fromJson(content);

    return success;
}

/*!
 * \brief Executes the HTTP request.
 *
 * The reply content will be filled by the content returned by the server
 * if the request succeeds, or by an error string if the request fails.
 *
 * \param replyContent the reply content.
 * \return true if the request succeeds, false otherwise.
 */
bool HttpClient::apply(QByteArray & replyContent)
{
    bool success;
    replyContent.clear();

    switch (m_httpMethod)
    {
        case HttpMethod::GET:
            success = doGet(replyContent);
            break;
        case HttpMethod::POST:
            success = doPost(replyContent);
            break;
        case HttpMethod::PUT:
            success = doPut(replyContent);
            break;
        case HttpMethod::DELETE:
            success = doDelete(replyContent);
            break;
        default:
            // This should never happen, the default method is GET.
            replyContent = "The HTTP method has not been set !";
            success = false;
    }

    return success;
}

/*!
 * \brief Returns the last error returned by the client.
 * \return may be invalid if you call this method too late.
 */
const QString & HttpClient::lastError() const
{
    return m_lastError;
}


//================ PRIVATE METHODS ================//

// The GET method
bool HttpClient::doGet(QByteArray & replyContent)
{

    QNetworkReply * reply = m_manager->get(QNetworkRequest(m_url));
    connectReply(reply);
    waitForFinish(reply);

    return fillReplyContent(reply, replyContent);
}

// The POST method
bool HttpClient::doPost(QByteArray & replyContent)
{
    QNetworkRequest request(m_url);
    request.setHeader(QNetworkRequest::ContentTypeHeader, "application/x-www-form-urlencoded");

    QNetworkReply * reply = m_manager->post(request, m_postData);
    connectReply(reply);
    waitForFinish(reply);

    return fillReplyContent(reply, replyContent);
}

// The PUT method
bool HttpClient::doPut(QByteArray & replyContent)
{
    QNetworkRequest request(m_url);

    QNetworkReply * reply = m_manager->put(request, m_postData);
    connectReply(reply);
    waitForFinish(reply);

    return fillReplyContent(reply, replyContent);
}

// The DELETE method
bool HttpClient::doDelete(QByteArray& replyContent)
{
    QNetworkReply * reply = m_manager->deleteResource(QNetworkRequest(m_url));
    connectReply(reply);
    waitForFinish(reply);

    return fillReplyContent(reply, replyContent);
}

// Connect the reply upload/download signals to the HTTP client upload/download
// signals
void HttpClient::connectReply(QNetworkReply * reply)
{
    connect(reply, &QNetworkReply::downloadProgress, this, &HttpClient::downloadProgress);
    connect(reply, &QNetworkReply::uploadProgress, this, &HttpClient::uploadProgress);
}

// If the request has succeeded, the replyContent is filled by
// the content returned by the server.
// If not, the replyContent if filled by the error string.
bool HttpClient::fillReplyContent(QNetworkReply * reply, QByteArray& replyContent)
{
    bool success = isSuccess(reply);
    if (success)
    {
        replyContent = reply->readAll();
    }
    else
    {
        replyContent = reply->attribute(QNetworkRequest::HttpReasonPhraseAttribute).toByteArray();
        m_lastError = QString(replyContent);
    }

    return success;
}

// Returns true if the request succeeded.
bool HttpClient::isSuccess(QNetworkReply * reply)
{
    return reply->error() == QNetworkReply::NoError;
}

// Waits for the request to finish
void HttpClient::waitForFinish(QNetworkReply * reply)
{
    QEventLoop loop;
    connect(reply, &QNetworkReply::finished, &loop, &QEventLoop::quit);
    loop.exec();
}

} // namespace http
