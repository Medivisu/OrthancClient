#ifndef ICOMMAND
#define ICOMMAND

#include <QObject>

#include "idynamicobject.h"

namespace core {

/*!
     * \brief Interface representing command results.
     */
class ICommandResults
{

    public:
        virtual ~ICommandResults() {}
};

/*!
 * \brief Interface representing a command.
 */
class ICommand : public IDynamicObject
{

    public:

        /*!
         * \brief Executes the command.
         * \returns true on success, false otherwise.
         */
        virtual bool execute() = 0;

        /*!
         * \brief Returns the results of this command.
         * If this command does not have any results, returns null.
         * \return the results of this command.
         */
        ICommandResults * results()  const
        {
            return m_results;
        }

    protected:

        /*!
         * \brief Creates a new command with given parameters.
         * \param params the command parameters, can be null.
         */
        ICommand() { m_results = nullptr; }
        virtual ~ICommand() = default;

        ICommandResults * m_results; // Stays null if no results are needed.
};

} // namespace Core


#endif // ICOMMAND

