#ifndef TOOLBOX_H
#define TOOLBOX_H

#include <QString>
#include <QByteArray>

#include "enumerations.h"


namespace toolbox
{



bool isExistingFile(const QString& path);
bool isExistingPath(const QString& path);

bool readFile(QString& content,
              const QString& path);


bool readFile(QByteArray& content,
              const QString& path);

bool writeFile(const QByteArray& content,
               const QString& path);
bool writeFile(const QByteArray& content,
               const QString& path,
               const QString& filename);

bool writeFilesToDirectory(const QString& path,
                           QVector<QByteArray> binaryData,
                           QStringList filenames);

bool removeFile(const QString& path);

bool createDirectory(const QString& path);
bool createPath(const QString & path);


qint64 getFileSize(const QString& path);

bool isWellFormedDicom(const QString& path);
bool isWellFormedDicom(const QByteArray& binaryData);





}

#endif // TOOLBOX_H
