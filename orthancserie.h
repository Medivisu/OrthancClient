#ifndef SERIES_H
#define SERIES_H

#include <QJsonDocument>
#include "orthancconnection.h"
#include "orthancinstance.h"

#include "iorthancresource.h"
#include "multithreading/threadedcommandprocessor.h"

namespace orthanc_client
{



/*!
 * \brief The serie class in the OrthancClient namespace corresponds to a
 * serie stored in an Orthanc Server.
 */
class OrthancSerie : public IOrthancResource,
        private core::ArrayFilledByThreads::IFiller
{
    public:

        // The main tags
        static const char MANUFACTURER[];
        static const char MODALITY[];
        static const char SERIES_DATE[];
        static const char SERIES_DESCRIPTION[];
        static const char SERIES_INSTANCE_UID[];
        static const char SERIES_NUMBER[];
        static const char SERIES_TIME[];

        OrthancSerie(orthanc_client::OrthancConnection& connection,
                     const QString& id);

        int getInstanceCount();
        OrthancInstance& getInstance(int index);
        OrthancInstance & getInstanceByID(const QString & id);

        QString anonymize(bool reload);
        QString anonymize(const QVariantMap & replace,
                          const QStringList & keep,
                          const QStringList & remove,
                          bool keepPrivateTags);

        void downloadAsZip(const QString & path, QString zipName);
        void downloadAsZip(const QString & path);
        void deleteResource() override;

    private:
        core::ArrayFilledByThreads m_instances;

        void readSerie();

        virtual int getFillerSize();
        virtual core::IDynamicObject * getFillerItem(int index);

};

} // namespace OrthancClient

#endif // SERIES_H
